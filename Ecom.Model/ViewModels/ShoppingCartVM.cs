﻿using Ecom.Models.Models;

namespace Ecom.Models.ViewModels
{
    public class ShoppingCartVM
    {
        public IEnumerable<ShoppingCart>? ShoppingCartList { get; set; }

        public ShoppingCart? ShoppingCart { get; set; }

        public OrderHeader OrderHeader { get; set; }
    }
}
