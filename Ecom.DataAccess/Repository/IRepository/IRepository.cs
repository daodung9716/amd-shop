﻿using System.Linq.Expressions;

namespace Ecom.DataAccess.Repository.IRepository
{
    public interface IRepository<T> where T : class//chỉ ra rằng T là 1 kiểu tham chiếu class, ko phải struct
    {
        //T - Category
        Task<IEnumerable<T>> GetAll(Expression<Func<T, bool>>? filter = null, string? includeProperties = null, bool tracked = false);
        Task<T> Get(Expression<Func<T, bool>> filter, string? includeProperties = null, bool tracked = false);
        Task Add(T entity);
        void Remove(T entity);
        void RemoveRange(IEnumerable<T> entity);
    }
}
